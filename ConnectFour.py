from colorama import Fore

board = [[" " for _ in range(7)] for _ in range(6)]
moves = [("Red", Fore.RED), ("Cyan", Fore.CYAN)] * 21

def printTitle():
    print("   +"+("-"*37)+"+   \n   |"+(" "*37)+"|   \n   |"+(" "*10)+"CLI ", end = "")
    print("Connect Four!"+(" "*10)+"|   \n   |"+(" "*37)+"|   \n   +"+("-"*37)+"+   ")

def printGameBoard(board):
    print("\n"+("="*45)+"\n")
    sep = "\t+" + "---+" * 7
    for row in board:
        print(sep + "\n\t", end = "")
        [print(f"| {cell} ", end = "") for cell in row] + [print("|")]
    print(sep + "\n\t| 1 | 2 | 3 | 4 | 5 | 6 | 7 |\n" + sep)

def columnIsAvailable(column):
    return column in [1, 2, 3, 4, 5, 6, 7] and board[0][column - 1] == " "

def addToken(column: int, colour: str) -> None:
    for i in range(6):
        if board[i][column - 1] == " ":
            pos = (i, column - 1)
    board[pos[0]][pos[1]] = colour + "@" + Fore.RESET

def gameWon(board):
    red, cyan = [Fore.RED + "@" + Fore.RESET] * 4, [Fore.CYAN + "@" + Fore.RESET] * 4
    ds, splits = [[[(x+i,y+i) for i in range(4)] for x in range(3)] for y in range(4)], []
    ds += [[[(x+i,y+j) for i,j in [(0,3),(1,2),(2,1),(3,0)]] for x in range(3)] for y in range(4)]
    for s in ds:
        for e in s:
            splits.append(e)
    splits = [[board[c[0]][c[1]] for c in d] for d in splits]
    for j in range(7):
        splits += [[board[i][j] for i in range(6)][i:i+4] for i in range(3)]
    for row in board:
        splits += [row[i:i+4] for i in range(4)]
    return red in splits or cyan in splits

[print() for _ in range(25)] + [print(u"\u001b[25A", end = "")]

printTitle()

for move in moves:
    printGameBoard(board)
    print()
    col = 0
    while col == 0:
        entry = input(f"Column ({move[1]}{move[0]}{Fore.RESET}) : ")
        try: pos = int(entry)
        except: pos = 0; print(u"\u001b[1A\u001b[2K", end = "")
        col = pos if columnIsAvailable(pos) else 0
    addToken(col, move[1])
    [print(u"\u001b[1A\u001b[2K", end = "") for _ in range(20)]
    if gameWon(board):
        printGameBoard(board)
        print(f"\n{move[1]}{move[0]}{Fore.RESET} has won!\n")
        exit()